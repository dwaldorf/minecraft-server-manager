﻿using Docker.DotNet;
using Docker.DotNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reactive.Threading.Tasks;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace minecraft_server_manager.Services
{
    public sealed class DockerMonitoringService
    {
        private static readonly Lazy<DockerMonitoringService> lazy =
            new Lazy<DockerMonitoringService>
                (() => new DockerMonitoringService());

        public static DockerMonitoringService Instance { get { return lazy.Value; } }

        private DockerClient _client;
        private Task _monitorTask;
        private ObservableProgress _progress = new ObservableProgress();
        private CancellationTokenSource _cts = new CancellationTokenSource();

        public DockerClient Client => _client;
        public IObservable<JSONMessage> Progress => _progress.ToObservable();

        private DockerMonitoringService()
        {

            // TODO: generalize connection
            // Damn no easy way to address both windows named pipes and unix
            // sockets although the docu says so
            _client = new DockerClientConfiguration(new Uri("npipe://./pipe/docker_engine"))
                .CreateClient();

            _monitorTask = Task.Run(async () => await _client.System.MonitorEventsAsync(new ContainerEventsParameters(), _progress, _cts.Token));
        }

        public async Task<IObservable<string>> GetLogStream(string id)
        {
            var stream = await _client.Containers.GetContainerLogsAsync(id, new ContainerLogsParameters { }, _cts.Token);
            int bufferSize = 4096;
            var buffer = new byte[bufferSize];

            return stream.ReadAsync(buffer, 0, bufferSize)
                .ToObservable().Select(bytesRead => Encoding.UTF8.GetString(buffer, 0, buffer.Length));
        }

        class ObservableProgress : IProgress<JSONMessage>
        {
            private Subject<JSONMessage> _progress = new Subject<JSONMessage>();

            private string _lastState = string.Empty;

            void IProgress<JSONMessage>.Report(JSONMessage value)
            {
                _progress.OnNext(value);
            }

            public IObservable<JSONMessage> ToObservable() =>
                _progress
                    .Where(state => state.ID != null && state.Status != _lastState);
                    //.DistinctUntilChanged(state => state.Status)  // Sadly doesn't work
        }
    }
}

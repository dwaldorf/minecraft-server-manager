﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using Docker.DotNet;
using Docker.DotNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace minecraft_server_manager.Controllers
{
    [Route("api/[controller]")]
    public class ServerController : ControllerBase
    {
        private IEnumerable<ServerInfo> _serverList = new List<ServerInfo>();
        private DockerClient _client;
        private CancellationTokenSource _cts = new CancellationTokenSource();

        private CompositeDisposable _disposables = new CompositeDisposable();
        private IHubContext<Hubs.ServerStateHub> _hubContext;

        public ServerController(IHubContext<Hubs.ServerStateHub> hubContext)
        {
            _hubContext = hubContext;

            try
            {
                _serverList = JsonConvert.DeserializeObject<IEnumerable<ServerInfo>>(
                    System.IO.File.ReadAllText(@"serverlist.json"));

                // TODO: generalize connection
                // Damn no easy way to address both windows named pipes and unix
                // sockets although the docu says so
                _client = Services.DockerMonitoringService.Instance.Client;

                _disposables.Add(Services.DockerMonitoringService.Instance.Progress.Subscribe(async state =>
                {
                    System.Console.WriteLine($"###################################### {state.Status}");
                    await _hubContext.Clients.All.SendAsync("UpdateState", state, _cts.Token);
                }));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<ServerInfo>> ListServers()
        {
            var result = new List<ServerInfo>();

            try
            {
                var containers = await _client.Containers.ListContainersAsync(
                    new ContainersListParameters { All = true });

                var r = from server in _serverList
                        join container in containers on server.Id equals container.ID
                        select new ServerInfo
                        {
                            Id = server.Id,
                            State = container.State,
                            Name = container.Image
                        };

                return r;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return result;
        }

        [HttpPost("[action]")]
        public async Task CreateServer()
        {
            try
            {
                var newname = System.Guid.NewGuid().ToString();
                var result = await _client.Containers.CreateContainerAsync(
                    new CreateContainerParameters
                    {
                        Image = "casenewmark/bedrock",
                        Name = newname
                    });
                (_serverList as List<ServerInfo>).Add(new ServerInfo
                {
                    Name = newname,
                    Id = result.ID
                });
                System.IO.File.WriteAllText(@"serverlist.json", JsonConvert.SerializeObject(_serverList));
                Console.WriteLine(result.ID);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        [HttpPut("StartServer/{id}")]
        public async Task StartServer(string id)
        {
            try
            {
                await _client.Containers.StartContainerAsync(id, new ContainerStartParameters(), _cts.Token);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine(id);
        }

        [HttpPut("StopServer/{id}")]
        public async Task StopServer(string id)
        {
            try
            {
                await _client.Containers.StopContainerAsync(id, new ContainerStopParameters(), _cts.Token);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine(id);
        }

        [HttpDelete("RemoveServer/{id}")]
        public async Task RemoveServer(string id)
        {
            try
            {
                await _client.Containers.RemoveContainerAsync(id, new ContainerRemoveParameters(), _cts.Token);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine(id);
        }

        public class ServerInfo
        {
            public string Name { get; set; }
            public string Id { get; set; }
            public string State { get; set; }
        }
    }
}

import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-create-server',
  templateUrl: './create-server.component.html',
  styleUrls: ['./create-server.component.css']
})
export class CreateServerComponent implements OnInit {

  constructor(public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) {
  }

  public ngOnInit() {
  }

  public createServer() {
    this.http.put(this.baseUrl + 'api/server/CreateServer', null).subscribe(result => {
      debugger;
    }, error => console.error(error));
  }
}

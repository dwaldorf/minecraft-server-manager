import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, interval } from 'rxjs';
import { map, flatMap, startWith } from 'rxjs/operators';
import { HubConnection, HubConnectionBuilder, LogLevel } from '@aspnet/signalr';

@Component({
  selector: 'app-list-servers',
  templateUrl: './list-servers.component.html'
})
export class ListServersComponent implements OnInit {
  public serverInfos: ServerInfo[];
  private _hubConnection: HubConnection;

  constructor(public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) { }

  public ngOnInit() {
    this.refreshServerList();

    this._hubConnection = new HubConnectionBuilder()
      .withUrl('/serverState')
      .configureLogging(LogLevel.Debug)
      .build();

    this._hubConnection
      .start()
      .then(() => console.log('Connection started!'))
      .catch(err => console.log('Error while establishing connection :('));

    this._hubConnection.on('UpdateState', (state: any) => {
      console.log(state);
      this.refreshServerList();
    });
  }

  public createServer() {
    this.http.post(`${this.baseUrl}api/server/createServer`, null).subscribe(result => {
      console.log(result);
    }, error => console.error(error));
  }

  public startServer(info: ServerInfo) {
    info.executing = true;
    this.http.put<string>(`${this.baseUrl}api/server/startServer/${info.id}`, null).subscribe(result => {
      info.executing = false;
    }, error => console.error(error));
  }

  public stopServer(info: ServerInfo) {
    info.executing = true;
    this.http.put<string>(`${this.baseUrl}api/server/stopServer/${info.id}`, null).subscribe(result => {
      console.log(result);
      info.executing = false;
    }, error => console.error(error));
  }

  public restartServer(info: ServerInfo) {
    info.executing = true;
    this.http.put<string>(`${this.baseUrl}api/server/restartServer/${info.id}`, null).subscribe(result => {
      console.log(result);
      info.executing = false;
    }, error => console.error(error));
  }

  public removeServer(info: ServerInfo) {
    info.executing = true;
    this.http.delete<string>(`${this.baseUrl}api/server/removeServer/${info.id}`).subscribe(result => {
      console.log(result);
      info.executing = false;
    }, error => console.error(error));
  }

  refreshServerList() {
    this.http.get<ServerInfo[]>(this.baseUrl + 'api/server/listServers')
      .subscribe(servers => this.serverInfos = servers);
  }
}

interface ServerInfo {
  name: string;
  id: string;
  state: string;
  executing: boolean;
}

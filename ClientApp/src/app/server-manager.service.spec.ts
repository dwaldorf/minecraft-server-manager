import { TestBed, inject } from '@angular/core/testing';

import { ServerManagerService } from './server-manager.service';

describe('ServerManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServerManagerService]
    });
  });

  it('should be created', inject([ServerManagerService], (service: ServerManagerService) => {
    expect(service).toBeTruthy();
  }));
});
